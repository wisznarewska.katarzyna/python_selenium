
import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager
from pages.test_arena.admin_panel_page import AdminPanelPage
from pages.test_arena.login_page import LoginPage
from pages.test_arena.project_page import ProjectPage
from utils.random_name_of_project import generate_random_text


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl/zaloguj')
    yield browser
    browser.quit()


def test_homework_selenium(browser):
    login_page = LoginPage(browser)
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')

    admin_panel_page = AdminPanelPage(browser)
    admin_panel_page.open_admin_panel()
    admin_panel_page.click_add_project_button()

    project_name = generate_random_text(6)
    prefix_name = generate_random_text(3)

    project_page = ProjectPage(browser)
    project_page.enter_project_details(project_name, prefix_name)

    project_page.search_project(project_name)
    list_of_titles = project_page.get_list_of_project_titles()

    assert project_name in list_of_titles
