from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class AdminPanelPage:
    def __init__(self, browser):
        self.browser = browser

    def open_admin_panel(self):
        wait = WebDriverWait(self.browser, 10)
        admin_panel_link = (By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration']")
        wait.until(EC.element_to_be_clickable(admin_panel_link)).click()

    def click_add_project_button(self):
        wait = WebDriverWait(self.browser, 10)
        add_project_button_locator = (By.CSS_SELECTOR, "a.button_link")
        add_project_button = wait.until(EC.element_to_be_clickable(add_project_button_locator))
        add_project_button.click()
