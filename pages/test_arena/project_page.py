from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def enter_project_details(self, name, prefix):
        project_name_field = WebDriverWait(self.browser, 10).until(
            EC.element_to_be_clickable((By.ID, "name")))
        project_name_field.send_keys(name)

        project_prefix_field = self.browser.find_element(By.ID, "prefix")
        project_prefix_field.send_keys(prefix)

        self.browser.find_element(By.ID, 'save').click()

    def search_project(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
        search_bar = self.browser.find_element(By.ID, 'search')
        search_button = self.browser.find_element(By.ID, 'j_searchButton')
        search_bar.send_keys(project_name)
        search_button.click()

    def get_list_of_project_titles(self):
        lists_of_project = self.browser.find_elements(By.CSS_SELECTOR, 'table tbody td a')
        list_of_titles = [i.text for i in lists_of_project]
        return list_of_titles
